const state = {
  token: null,
  weather: null
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_WEATHER: (state, weather) => {
    state.weather = weather
  }
}

const actions = {
  async setToken ({commit}, token) {
    commit('SET_TOKEN', token)
  },
  setWeather ({commit}, weather) {
    commit('SET_WEATHER', weather)
  }
}

export default {
  state,
  mutations,
  actions
}
