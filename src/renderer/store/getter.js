const getters = {
  token: state => state.app.token,
  weather: state => state.app.weather
}
export default getters
