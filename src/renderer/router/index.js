import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: require('@/view/home').default
    },
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/weather',
      name: 'weather',
      component: () => import('@/view/weather')
    },
    {
      path: '/setting',
      name: 'setting',
      component: () => import('@/view/setting')
    },
    {
      path: '/sa',
      name: 'sa',
      component: () => import('@/view/conversation')
    }
  ]
})
