import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import vueAplayer from 'vue-aplayer'
import VueAMap from 'vue-amap'
// import CognitiveServicesCredentials from '@azure/ms-rest-azure-js'
// import WebSearchAPIClient from '@azure/cognitiveservices-websearch'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.prototype.$axios = axios
Vue.config.productionTip = false
Vue.use(ElementUI)
// Vue.use(vueAplayer)
Vue.use(VueAMap)
VueAMap.initAMapApiLoader({
  key: '142566f6064108a30b1e26b72cc901a2',
  plugin: ['AMap.Autocomplete', 'AMap.PlaceSearch', 'AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PolyEditor', 'AMap.CircleEditor'],
  v: '1.4.4',
  uiVersion: '1.0.11' // 版本号
})

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
