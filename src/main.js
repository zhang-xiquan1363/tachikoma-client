import Vue from 'vue'
import App from './App.vue'
import VueAMap from 'vue-amap'
Vue.config.productionTip = false
Vue.use(VueAMap)
VueAMap.initAMapApiLoader({
  key: '142566f6064108a30b1e26b72cc901a2',
  plugin: ['AMap.Autocomplete', 'AMap.PlaceSearch', 'AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PolyEditor', 'AMap.CircleEditor'],
  v: '1.4.4',
  uiVersion: '1.0.11' // 版本号
})

new Vue({
  render: h => h(App)
}).$mount('#app')
