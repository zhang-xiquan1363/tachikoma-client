const IS_PROD = ['production', 'prod'].includes(process.env.NODE_ENV)
const path = require('path')
function resolve (dir) {
  // __dirname为项目根目录，node的express方法中也有使用，join为javascript连接符
  return path.join(__dirname, dir)
}
module.exports = {
  // 默认'/'，部署应用包时的基本 URL
  publicPath: IS_PROD ? process.env.VUE_APP_PUBLIC_PATH : './',

  // outputDir: process.env.outputDir || 'dist', // 'dist', 生产环境构建文件的目录
  // assetsDir: "", // 相对于outputDir的静态资源(js、css、img、fonts)目录
  lintOnSave: false,

  // 是否使用包含运行时编译器的 Vue 构建版本
  runtimeCompiler: true,

  // 生产环境的 source map
  productionSourceMap: !IS_PROD,

  parallel: require('os').cpus().length > 1,
  pwa: {},

  devServer: {
    overlay: {
      // 让浏览器 overlay 同时显示警告和错误
      warnings: true,
      errors: true
    },
    open: true, // 是否打开浏览器
    host: 'localhost',
    port: '8082', // 代理断就
    https: false,
    hotOnly: true // 热更新
    /* proxy: {
      "/api": {
        target:
          "https://www.easy-mock.com/mock/5bc75b55dc36971c160cad1b/sheets", // 目标代理接口地址
        secure: false,
        changeOrigin: true, // 开启代理，在本地创建一个虚拟服务端
        // ws: true, // 是否启用websockets
        pathRewrite: {
          "^/api": "/"
        }
      }
    } */
  },
  assetsDir: 'assets',
  // chainWebpack这里是比较重要的
  /*
  chainWebpack: (config) => {
    config.resolve.alias
      .set('@', resolve('src'))
      .set('assets', resolve('src/assets'))
  }, */
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        // 加上自己的文件路径，不能使用别名
        path.resolve(__dirname, 'src/assets/css/style.scss')
      ]
    }
  }
}
